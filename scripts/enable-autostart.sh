#!/bin/bash

FILENAME="myway-bluetooth-setup.desktop"
AUTOSTART_DIR="/home/pi/.config/autostart"

#MOVE out of /scripts folder
cd ..
#Get working directory
DIR="$( pwd )"

command -v xterm >/dev/null 2>&1 || { echo >&2 "xterm needs to be installed in order to autostart. Please install with 'sudo apt-get install xterm' then rerun this script."; exit 1; }

if [ ! -d $AUTOSTART_DIR ]; then
	echo "Directory '$AUTOSTART_DIR' does not exist"
	exit 1
fi

if [ -e $AUTOSTART_DIR/$FILENAME ]; then
	echo "Autostart already enabled"
	exit 1
fi

echo "[Desktop Entry]" >> $AUTOSTART_DIR/$FILENAME
echo "Encoding=UTF-8" >> $AUTOSTART_DIR/$FILENAME
echo "Name=MyWay Bluetooth Setup Auto-Start on boot" >> $AUTOSTART_DIR/$FILENAME
echo "Comment=Start a terminal and the bluetooth config utility." >> $AUTOSTART_DIR/$FILENAME
echo "Exec=/usr/bin/xterm -maximized -e '/usr/local/bin/node $DIR/index.js'" >> $AUTOSTART_DIR/$FILENAME
echo "" >> $AUTOSTART_DIR/$FILENAME

echo "Autostart is now enabled."

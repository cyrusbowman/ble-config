#!/bin/bash

FILENAME="myway-bluetooth-setup.desktop"
DESKTOP_DIR="/home/pi/Desktop"

if [ ! -d $DESKTOP_DIR ]; then
	echo "Directory '$DESKTOP_DIR' does not exist"
	exit 1
fi

if [ ! -e $DESKTOP_DIR/$FILENAME ]; then
	echo "Shortcut already removed"
	exit 1
fi

rm $DESKTOP_DIR/$FILENAME
echo "Shortcut is now removed."

#!/bin/bash

FILENAME="myway-bluetooth-setup.desktop"
DESKTOP_DIR="/home/pi/Desktop"

#MOVE out of /scripts folder
cd ..
#Get working directory
DIR="$( pwd )"

if [ ! -d $DESKTOP_DIR ]; then
	echo "Directory '$DESKTOP_DIR' does not exist"
	exit 1
fi

if [ -e "$DESKTOP_DIR/$FILENAME" ]; then
	echo "Shortcut already created"
	exit 1
fi

echo "[Desktop Entry]" >> $DESKTOP_DIR/$FILENAME
echo "Name=MyWay Bluetooth Setup Utility" >> $DESKTOP_DIR/$FILENAME
echo "Comment=A utility to configure Serial IO bluetooth adapters." >> $DESKTOP_DIR/$FILENAME
echo "Icon=$DIR/icon.xpm" >> $DESKTOP_DIR/$FILENAME
echo "Exec=/usr/bin/xterm -maximized -e '/usr/local/bin/node $DIR/index.js'" >> $DESKTOP_DIR/$FILENAME
echo "Type=Application" >> $DESKTOP_DIR/$FILENAME
echo "Encoding=UTF-8" >> $DESKTOP_DIR/$FILENAME
echo "Terminal=false" >> $DESKTOP_DIR/$FILENAME
echo "Categories=None;" >> $DESKTOP_DIR/$FILENAME
echo "" >> $DESKTOP_DIR/$FILENAME

echo "Shortcut was created."
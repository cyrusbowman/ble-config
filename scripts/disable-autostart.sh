#!/bin/bash

FILENAME="myway-bluetooth-setup.desktop"
AUTOSTART_DIR="/home/pi/.config/autostart"

if [ ! -d $AUTOSTART_DIR ]; then
	echo "Directory '$AUTOSTART_DIR' does not exist"
	exit 1
fi

if [ ! -e $AUTOSTART_DIR/$FILENAME ]; then
	echo "Autostart already disabled"
	exit 1
fi

rm $AUTOSTART_DIR/$FILENAME
echo "Autostart is now disabled."

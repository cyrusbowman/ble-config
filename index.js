var SerialPort = require("serialport");
var _ = require('lodash');
var Promise = require("bluebird");
var inquirer = require("inquirer");
var fs = require('fs');
var serial = null;
var recvBuffer = '';
var configuring = false;

var notice = "------ PRESS ANY KEY TO CONFIGURE BLUETOOTH ADAPTER ------";
var USB_PATH = "/dev/ttyUSB0";

function startMenu() {
	return menu().then(function() {
		startMenu();
	});
}

function _configure() {
	try {
		fs.accessSync(USB_PATH, fs.F_OK);
	} catch (e) {
		console.log("Please plug in USB to Serial cable.");
		return new Promise.resolve();
	}

	console.log('Finding baud rate...');
	return findBaud().then(function(found) {
		if (found == true) {
			//Turn on bluetooth command change mode
			return _turnOnBLECommandMode().then(() => {
				return _getBluetoothAddress();
			}).then(function(bluetoothAddress) {
				console.log('**********************   Bluetooth Address:', bluetoothAddress, '   **********************');
				return _setName('MyWay-'+bluetoothAddress.substr(-2)).then(function(newName) {
					console.log('New Name: ', newName);
				});
			}).catch(() => {
				console.log('Failed to configure bluetooth device.');
			}).then(() => {
				return closeSerial();
			});
		} else {
			console.log('Baud not found. Is device plugged in and on?');
			return closeSerial().catch(function(err) {
				//err
			});
		}
	}).catch((err) => {
		console.log('Error:', err.message);
		return closeSerial();
	})
}


function openSerial(baud) {
	return new Promise(function(resolve){
		serial = new SerialPort(USB_PATH, {baudrate: parseInt(baud)});
		recvBuffer = '';
		serial.on("open", function() {
		    serial.on('error', function(err) {
		        console.log('Error: ', err);
		        throw err;
		    });
		    serial.on('data', function(buffer) {
				recvBuffer = recvBuffer + buffer.toString();
			});
			resolve();
		});
	});
}
function closeSerial() {
	if (serial == null) return Promise.resolve();
	return new Promise(function(resolve){
		recvBuffer = '';
		serial.close(function() {
			resolve();
		});
	});
}

function findBaud() {
	//Loop through bauds until we find one that gets a response
	var bauds = ['115200', '38400', '19200', '9600',  '57600', '4800'];
	var found = false;
	return Promise.each(bauds, function(baud) {
		if (found == true) {
			return;
		}
		//Try first baud
		return closeSerial().then(function() {
			return openSerial(baud);
		}).then(function() {
			//Change to command mode
			return sendData('+++\r\n').then(() => {
				return Promise.delay(100)
			}).then(() => {
				return sendAndRecv('AT\r\n', 500);
			}).then(function(response) {
				if (response.indexOf('OK') != -1) {
					console.log('Baud is (' + baud + ')');
					found = true;
				}
			});
		}).catch(function(err) {
			//Not this baud
			return false;
		});
	}).then(function() {
		if (found == true) {
			return true;
		}
		return false;
	}).catch(function(err) {
		return false;
	});
}
function _turnOnBLECommandMode() {
	return sendAndRecv('AT+MODESWITCHEN=ble,1\r\n', 1000).then(function(response) {
		if (response.indexOf('OK') != -1) {
			return true;
		} else {
			throw 'Failed to turn on bluetooth command mode.'
		}
	});
}
function _getBluetoothAddress() {
	return sendAndRecv('AT+BLEGETADDR\r\n', 1000).then(function(response) {
		//var re = /get bl a\r\nR[0-9]+\r\n([A-F]+)/g;
		var re = /(([0-9]|[A-F]|\:)+)\r\nOK/g;
		var result = re.exec(response);
		if (result != null && result.length > 0) {
			return result[1];
		} else {
			throw 'Could not find bluetooth address.';
		}
	});
}
function _setName(name) {
	return sendAndRecv('AT+GAPDEVNAME='+_.trim(name) + '\r\n', 1000).then(function(response) {
		//Wait 100ms
		return Promise.delay(100).then(() => {
			//Send reboot
			return sendAndRecv('ATZ\r\n', 1000).then(function() {
				//Get name after 1 second
				return Promise.delay(500).then(() => {
					//Change back to command mode
					return sendData('+++\r\n').then(() => {
						return Promise.delay(100)
					})
				}).then(function() {
					return sendAndRecv('AT+GAPDEVNAME\r\n', 1000).then(function(response) {
						var re = /(MyWay-([0-9]|[A-F]){2})\r\nOK/g;
						var result = re.exec(response);
						var newName = 'Failed to set name.';
						if (result != null && result.length > 0) {
							newName =  _.trim(result[1]);
						} else {
							throw 'Failed to set name.';
						}
						return newName;
					});
				});
			});
		})
	});
}


function sendAndRecv(command, timeout) {
	return Promise.try(function() {
		//Clear recv buffer
		recvBuffer = '';
		//Send command.
		return sendData(command).then(function() {
			return waitForResponse(timeout);
		});
	});
}

function waitForResponse(maxWait) {
	return Promise.try(function() {
		while (recvBuffer.length == 0) {
			if (maxWait == null || maxWait > 0) {
				return Promise.delay(100).then(function() {
					var newWait = maxWait;
					if (newWait != null) {
						newWait = newWait - 100;
					}
					return waitForResponse(newWait);
				});
			} else {
				throw new Error('Timeout');
				return;
			}
		}
		return recvBuffer;
	});
}

function sendData(data) {
    return new Promise(function(resolve){
        var buffer = new Buffer(data, "ascii");
        serial.write(buffer, function(err, bytesWritten) {
            if (err) console.log(err);
            if(bytesWritten < data.length) {
                console.log('Warning: Did not write all bytes... ' + bytesWritten + '/' + data.length + ' written.');
            }
            resolve(bytesWritten);
        });
    });
}

function menu() {
	return inquirer.prompt([
		{
			type: "list",
			name: "todo",
			message: "What do you want to do?",
			choices: [
				"Configure Bluetooth Adapter"
			]
		}
		], 	function( answers ) {
				if (answers.todo == 'Configure Bluetooth Adapter') {
					return _configure();
				}
			}
		);
}


var keypress = require('keypress');
console.log("\r\n" + notice);
// make `process.stdin` begin emitting "keypress" events
keypress(process.stdin);
// listen for the "keypress" event
process.stdin.on('keypress', function (ch, key) {
  if (key && key.ctrl && key.name == 'c') {
  	//Allow stop
    process.stdin.pause();
  } else {
  	if (configuring == false) {
  		configuring = true;
  		console.log();
  		return _configure().then(function() {
  			configuring = false;
  			console.log("\r\n" + notice);
  		});
  	}
  }
});

process.stdin.setRawMode(true);
process.stdin.resume();

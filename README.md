Bluetooth Serial IO Adapter Setup
----

To run:
    `npm start`


Auto-start on RPi
---
- Navigate to `scripts` directory
- To enable auto-start run `sh enable-autostart.sh`
- To disable auto-start run `sh disable-autostart.sh`

Desktop shortcut on RPi
---
- Navigate to `scripts` directory
- To create shortcut run `sh add-desktop-shortcut.sh`
- To remove shortcut run `sh remove-desktop-shortcut.sh`

Enable autostart manually on RPi
---
- Install xterm `sudo apt-get install xterm`
- Open `/home/pi/.config/autostart/myway-bluetooth-setup.desktop`
- Add the following lines:

		[Desktop Entry]
	    Encoding=UTF-8
	    Name=Terminal autostart
	    Comment=Start a terminal and the bluetooth config utility.
	    Exec=/usr/bin/xterm -maximized -e '/usr/local/bin/node /home/pi/MyWay/Bluetooth_Serial_IO_Setup/index.js`

- Save the file.
